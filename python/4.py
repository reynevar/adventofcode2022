#!/usr/bin/python3

with open('../inputs/4') as f:
    listSectionsStr = f.read().strip().splitlines()

def anyOverlap(r1, r2):
    doesOverlap = int(r1[0])<= int(r2[1]) and int(r2[0]) <= int(r1[1])
    return (doesOverlap)

def fullOverlap(r1, r2):
    doesOverlap = int(r1[0])>= int(r2[0]) and int(r1[1]) <= int(r2[1]) or int(r2[0])>= int(r1[0]) and int(r2[1]) <= int(r1[1])
    return (doesOverlap)

anyOverlapSum = 0
fullOverlapSum = 0
for line in listSectionsStr:
    s1,s2 = line.split(',')
    s1 = s1.split('-')
    s2 = s2.split('-')
    if fullOverlap(s1, s2):
        fullOverlapSum += 1
    if anyOverlap(s1, s2):
        anyOverlapSum += 1


print('Solution 4a: ' + str(fullOverlapSum))
print('Solution 4a: ' + str(anyOverlapSum))
