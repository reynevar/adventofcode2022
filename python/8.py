#!/usr/bin/python3
from math import prod

with open('../inputs/8') as f:
    treeList = [list(map(int, x.strip())) for x in f.readlines()]

rows, cols = len(treeList), len(treeList[0])
visible = 2*rows + 2*cols - 4
for y in range(1, cols - 1):
    for x in range(1, rows - 1):
        #get highest number in given direction
        up = max(col[x] for col in treeList[:y])
        down = max(col[x] for col in treeList[y+1:])
        left = max(treeList[y][:x])
        right = max(treeList[y][x+1:])
        treeVal = treeList[y][x]
        if any(treeVal > others for others in [up,down,left,right]):
            visible += 1
print('Solution 8a: ', visible)

bestScore = -1
for y in range(1, cols - 1):
    for x in range(1, rows - 1):
        treeVal = treeList[y][x]
        #get score for numbers in given direction
        up = [treeVal > elem for elem in list(reversed([col[x] for col in treeList[:y]]))]
        down = [treeVal > elem for elem in list(col[x] for col in treeList[y+1:])]
        left = [treeVal > elem for elem in list(reversed(treeList[y][:x]))]
        right = [treeVal > elem for elem in list(treeList[y][x+1:])]

        scores = []
        for vals in [up,down,left,right]:
            if False in vals:
                scores.append(vals.index(False)+1)
            else:
                scores.append(len(vals))
        tmpScore = prod(scores)
        if tmpScore > bestScore:
            bestScore = tmpScore
print('Solution 8b: ', bestScore)
