#!/usr/bin/python3
from copy import deepcopy

def moveHead(head, direction):
    match(direction):
        case 'U': head[1] += 1
        case 'D': head[1] -= 1
        case 'R': head[0] += 1
        case 'L': head[0] -= 1
    return head

def moveTail(head, tail):
    xdiff = head[0] - tail[0]
    ydiff = head[1] - tail[1]
    if abs(xdiff) > 1 or abs(ydiff) >1:
        if xdiff == 0:
            tail[0] = deepcopy(head[0])
            tail[1] += (1 if ydiff>=1 else -1)
        elif ydiff == 0:
            tail[0] += (1 if xdiff>=1 else -1)
            tail[1] = deepcopy(head[1])
        else:
            tail[0] += (1 if xdiff>=1 else -1)
            tail[1] += (1 if ydiff>=1 else -1)
    return tail

with open('../inputs/9') as f:
    moveList = [x.strip().split(' ') for x in f.readlines()]

posListTail = set()
posList9thTail = set()
knotNumbers = 9

head = [0,0]
tail = [0,0]
tailParts = [[0,0] for _ in range(9)]

posListTail.add(tuple(tail))
posList9thTail.add(tuple(tail))

for direction, value in moveList:
    for i in range(int(value)):
        moveHead(head,direction)
        moveTail(head, tail)
        posListTail.add(tuple(tail))

        for t in range(knotNumbers):
            moveTail(head if t == 0 else tailParts[t-1], tailParts[t])
            if t == knotNumbers-1:
                posList9thTail.add(tuple(tailParts[t]))

print('Solution 9a: ', len(posListTail))
print('Solution 9b: ', len(posList9thTail))
