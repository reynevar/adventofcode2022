#!/usr/bin/python3
from pathlib import Path

with open('../inputs/7') as f:
    commandsList = [cmd.strip() for cmd in f.readlines()]

def getDirSize(dirPath, dirTree):
    totalSize = 0
    for obj in dirTree[dirPath]:
        if isinstance(obj, tuple):
            totalSize += obj[1]
        elif isinstance(obj, Path):
            totalSize += getDirSize(obj, dirTree)
    return totalSize

dirTree = {}
currDir = Path("")
for cmd in commandsList:
    if cmd.startswith("$ cd"):
        cmdList = cmd.split(' ')
        if cmdList[2] == '..':
            currDir = currDir.parent
        else:
            currDir = currDir / cmdList[2]
            if currDir not in dirTree:
                dirTree[currDir] = []
    else:
        cmdList = cmd.split(" ")
        if cmdList[0] == 'dir':
            dirTree[currDir].append(currDir/cmdList[1])
        elif cmdList[0].isnumeric():
            dirTree[currDir].append((cmdList[1], int(cmdList[0])))

dirSizeDict = {dirPath: getDirSize(dirPath, dirTree) for dirPath in dirTree}
dirSizeWithLimit = sum(dirSize for dirSize in dirSizeDict.values() if dirSize < 100000)
print('Solution 7a: ', dirSizeWithLimit)

totalSpace = 70000000
currentFreeSpace = totalSpace - dirSizeDict[Path("/")]
requiredSpaceToRemove = 30000000 - currentFreeSpace

selectedDir =(totalSpace, Path(" "))

print (dirSizeDict)
for dir in dirSizeDict:
    currDirSpace = dirSizeDict[dir]
    if currDirSpace >= requiredSpaceToRemove and currDirSpace < selectedDir[0]:
        selectedDir = (currDirSpace, dir)

print('Solution 7b: ', selectedDir[0])

