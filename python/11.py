#!/usr/bin/python3
from math import lcm

class Monkey:
    def __init__(self, data):
        self.inspections = 0
        for line in data.split('\n'):
            if 'items' in line:
                self.itemlist = [int(i) for i in line.split(': ')[1].split(', ')]
            elif 'Operation' in line:
                self.operationType = 'add'
                if '*' in line:
                    self.operationType = 'mul'
                self.operationVal = line.split(' ')[-1]
                if self.operationVal == 'old':
                    self.operationType = 'square'
                    self.operationVal = -1
                self.operationVal = int(self.operationVal)
            elif 'Test' in line:
                self.testVal = int(line.split(' ')[-1])
            elif 'true' in line:
                self.firstMonkey = int(line.split(' ')[-1])
            elif 'false' in line:
                self.secondMonkey = int(line.split(' ')[-1])
            elif 'Monkey' in line:
                self.monkeyNum = int(line.split(':')[0][-1])

    def printData(self):
        print('Num: ', self.monkeyNum)
        print('items: ', self.itemlist)
        print('operation: ', self.operationType, self.operationVal)
        print('test: ', self.testVal)
        print('monkeys: ', self.firstMonkey, self.secondMonkey)

    def printItems(self):
        print('Monkey ', self.monkeyNum, ': ', self.itemlist)

    def addItem(self, item):
        self.itemlist.append(item)

    def hasItems(self):
        return bool(self.itemlist)
    
    def processItem(self, rounds, common_div):
        if not self.itemlist:
            return -1, -1
        self.inspections += 1
        item = int(self.itemlist.pop(0))
        if self.operationType == 'add':
            worryLvl = int((item + self.operationVal))
        elif self.operationType == 'square':
            worryLvl = int((item * item))
        else:
            worryLvl = int((item * self.operationVal))
        worryLvl %= common_div
        worryLvl = worryLvl // 3 if rounds == 20 else worryLvl
        throwTo = self.firstMonkey if worryLvl % self.testVal == 0 else self.secondMonkey
        return throwTo, worryLvl

    def getInspections(self):
        return self.inspections


with open('../inputs/11') as f:
    monkeyList = f.read().split('\n\n')

mList = []
for m in monkeyList:
    monkey = Monkey(m)
    mList.append(monkey)

numbers = []
for monkey in mList:
    numbers.extend(monkey.itemlist)

common_div = lcm(*numbers)

rounds = 20
for i in range(rounds):
    for ml in mList:
        while ml.hasItems():
            nextMonkey, item = ml.processItem(rounds, common_div)
            mList[nextMonkey].addItem(item)
  #  print("ROUND ", i)
for m in mList:
    print(m.monkeyNum, m.getInspections())
  #      m.printItems()

monkeyBusiness = [-1, -1]
for m in mList:
    if m.getInspections() > monkeyBusiness[0]:
        monkeyBusiness.pop(0)
        monkeyBusiness.append(m.getInspections())
        monkeyBusiness.sort()

print('Solution 11a: ', monkeyBusiness[0]*monkeyBusiness[1])
