#!/usr/bin/python3

def findMsgStart(sig, msgLen):
    msgStart = 0
    msgEnd = -1
    for i in range(msgLen, len(signal)):
        testStr = sig[msgStart:i]
        if len(set(testStr)) == msgLen:
             msgEnd = i
             break
        else:
             msgStart += 1
    return msgEnd

with open('../inputs/6') as f:
    signal = f.read()

print('Solution 6a: ',findMsgStart(signal, 4))
print('Solution 6b: ',findMsgStart(signal,14))
