#!/usr/bin/python3
from collections import Counter

def getPrio(c):
    decrease = 96 # 97 is ascii 'a'
    if c.isupper():
        decrease = 38 # 65 - 27 (65 is ascii for 'A', prio shoud start at nr 27)
    return ord(c)-decrease

def getCommonChar(str1, str2):
    coll1 = Counter(set(str1))
    coll2 = Counter(set(str2))
    common = coll1 & coll2
    return ''.join(list(common.elements()))

# divide the list to n sublists, then use zip to aggregate elements
def groupElements(lst, n):
    return zip(*[lst[i::n] for i in range(n)])

with open('../inputs/3') as f:
    sacks = f.read().split('\n')[:-1]

prioSum = 0
for sack in sacks:
    halfLen = int(len(sack)/2)
    c1, c2 = sack[:halfLen], sack[halfLen:]
    commonChar = getCommonChar(c1,c2)
    prio = getPrio(commonChar)
    prioSum += prio

print('Answer 3a: ' + str(prioSum))

prioSum = 0
groupedSacks = groupElements(sacks,3)
for sack3 in groupedSacks:
    commonChar = getCommonChar(sack3[0], sack3[1])
    commonChar = getCommonChar(commonChar, sack3[2])
    prioSum += getPrio(commonChar)

print('Answer 3b: ' + str(prioSum))
