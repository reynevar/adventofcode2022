#!/usr/bin/python3

def getSignal(currCycle, currVal, signalsStr):
    if currCycle in [20,60,100,140,180,220]:
        signalsStr.append(currCycle * currVal)

def updatePixel(currCycle, currVal, imageList):
    if (currCycle-1)%40 in range(currVal-1,currVal+2):
        imageList[currCycle-1] = '#'

with open('../inputs/10') as f:
    instructionList = f.readlines()

signalsStr = []
currVal = 1
currCycle = 1
imageList = list('.' *40 * 6)
for inst in instructionList:
    currCycle += 1
    updatePixel(currCycle,currVal, imageList)
    getSignal(currCycle, currVal, signalsStr)
    if 'addx' in inst:
        currCycle +=1
        currVal += int(inst.split()[1])
        updatePixel(currCycle,currVal, imageList)
    if 'addx' in inst:
        getSignal(currCycle, currVal, signalsStr)

print('Solution 10a: ', sum(signalsStr))
print('solution 10b:\n')
for i in range(0, 240, 40):
    print(''.join(imageList[i: i+40]))
