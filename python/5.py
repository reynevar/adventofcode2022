#!/usr/bin/python3

with open('../inputs/5') as f:
     startStack, moves= f.read().split('\n\n')

def getElems(stack, count, rev = True):
    pos = -1*count
    popped = stack[pos:]
    del stack[pos:]
    if rev:
        popped.reverse()
    return popped

columns = len(startStack.split('\n')[-1].strip().split('   '))
crates = startStack.split('\n')[:-1]
rows = len(startStack.split('\n'))-1
stacks= []
stacksB = []
for i in range(columns):
    stack = []
    for j in range(rows):
        elem = crates[j][i*4+1]
        if elem != ' ':
            stack.append(elem)
    stack.reverse()
    stacks.append(stack)

for s in stacks:
    stacksB.append(s.copy())

for command in moves.strip().split('\n'):
    command = command.strip().split(' ')
    count = int(command[1])
    fromCol = int(command[3])-1
    toCol = int(command[5])-1
    elems = getElems(stacks[fromCol], count)
    stacks[toCol].extend(elems)
    elemsB = getElems(stacksB[fromCol], count, False)
    stacksB[toCol].extend(elemsB)

print('Solution 5a: ', ''.join([stacks[c].pop() for c in range(columns)]))
print('Solution 5b: ', ''.join([stacksB[c].pop() for c in range(columns)]))
