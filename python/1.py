#!/usr/bin/python3

with open('../inputs/1') as f:
    listCaloriesStr = f.readlines()

currItems = 0
itemList = []
for i in listCaloriesStr:
    if(i == '\n'):
        itemList.append(currItems)
        currItems = 0
    else:
        currItems += int(i)
itemList.sort()
print("part1: " + str(itemList[-1]))
print("part2: " + str(sum(itemList[-3:])))
