#!/usr/bin/python3

from collections import deque

def inRange(row, col, rows, cols):
    return row >=0 and row < rows-1 and col >= 0 and col < cols-1

def getNeighbours(board, point):
    x_min = y_min = 0
    x_max = len(board) -1
    y_max = len(board[0]) -1
    x,y = point

    point_height = board[x][y] -2

    neighbors = []

    if (x_min < x) and (point_height < board[x - 1][y]):
        neighbors.append((x - 1, y))

    if (x < x_max) and (point_height < board[x + 1][y]):
        neighbors.append((x + 1, y))

    if (y_min < y) and (point_height < board[x][y - 1]):
        neighbors.append((x, y - 1))

    if (y < y_max) and (point_height < board[x][y + 1]):
        neighbors.append((x, y + 1))

    return neighbors

def bfs(board, end):
    q = [end]
    level = dict()
    level[end] = 0

    while q:
        currPos = q.pop(0)
        currVal = board[currPos[0]][currPos[1]]
        print(currPos, currVal)

        for newPoint in getNeighbours(board, currPos):
            if newPoint not in level:
                level[newPoint] = level[currPos] + 1
                q.append(newPoint)
    return level

start_pos = None
end_pos = None
board = []
starts = []
with open('../inputs/12') as f:
    for x,line in enumerate(f):
        board.append([])
        for y,char in enumerate(line.strip()):
            if char == 'S':
                start_pos = (x,y)
                char = 'a'
            elif char == 'E':
                end_pos = (x,y)
                char = 'z'
            if char == 'a':
                starts.append((x,y))
            board[-1].append(ord(char))
answers = bfs(board, end_pos)
best = min([answers.get(point) for point in starts if answers.get(point)])
print('Solution 12a: ', answers[start_pos])
print('Solution 12b: ', best)
