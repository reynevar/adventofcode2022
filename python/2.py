#!/usr/bin/python3

scoreMap = {'AA': [4,4], 
            'AB': [8,1],
            'AC': [3,7],
            'BB': [5,5],
            'BC': [9,2],
            'CC': [6,6]
            }

strategyMap = {
        'X' : {'A': 'C', 'B': 'A', 'C' : 'B'}, # to lose
        'Y' : {'A': 'A', 'B': 'B', 'C' : 'C'}, # to draw
        'Z' : {'A': 'B', 'B': 'C', 'C' : 'A'}  # to win
        }

def getScore(lst):
    return scoreMap[''.join(sorted(lst))][lst[0]>lst[1]]

with open('../inputs/2') as f:
    strategyGuideStr = f.read().split('\n')[:-1]
scorePartA = 0
scorePartB = 0
for match in strategyGuideStr:
    itemsA = match.split(' ')
    itemsB = itemsA.copy()
    itemsA[1] = chr(ord(itemsA[1])-23)
    scorePartA += getScore(itemsA)
    
    itemsB[1] = strategyMap[itemsB[1]][itemsB[0]]
    scorePartB += getScore(itemsB)
print('Respons 2a: ' + str(scorePartA))
print('Respons 2B: ' + str(scorePartB))
